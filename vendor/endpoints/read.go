package endpoints

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

//Event View
func Read(c *gin.Context) {
	c.HTML(http.StatusOK, "main/read", gin.H{
		"title": "Online Preview",
	})
}
