package model

import (
	"github.com/jinzhu/gorm"
	"html/template"
)

//DataProfile model struct
type DataProfile struct {
	gorm.Model
	Icon    string
	Name    string `gorm:"unique"`
	Job     string
	Content template.HTML
}
